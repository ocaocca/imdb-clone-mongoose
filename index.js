const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const ObjectId = require('mongodb').ObjectID;
const multer = require('multer');
mongoose.set('useFindAndModify', false);

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname)
    }
});

const storageuser = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './useruploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname)
    }
});

const upload = multer({storage: storage});
const userupload = multer({storage: storageuser});

// make connection
mongoose.connect('mongodb://localhost/blog', {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useCreateIndex', true);

// MODEL
// const usertypeSchema = mongoose.Schema({
//     // code here
//     usertype: { type: Number},
//     usertypedesc: { type: String },
//   })
//   const usertypeModel = mongoose.model('usertype', usertypeSchema)

const reviewSchema = mongoose.Schema({
  // code here
  review: { type: String },
  rating: { type: Number },
//   userid: [{
//     type: mongoose.Schema.Types.ObjectId,
//     ref: userModel
//   }]
})
const reviewModel = mongoose.model('review', reviewSchema)

const movieSchema = mongoose.Schema({
  title: { type: String },
  releaseyear: { type: Date },
  releasedate: { type: Date },
  runtime: { type: Number },
  genre: { type: String },
  director: { type: String },
  writer: { type: String },
  actor: { type: String },
  plotsummary: { type: String },
  language: { type: String },
  country: { type: String },
  //movieposter: { type: String },
  movieposter: { data: Buffer, contentType: String },
  type: { type: String },
  studio: { type: String },
  website: { type: String },
  review: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: reviewModel
  }]
});
const movieModel = mongoose.model('movie', movieSchema);

const userSchema = mongoose.Schema({
  username: {type: String, unique: true},
  password: {type: String},
  email: {type: String},
  profilepicture: { data: Buffer, contentType: String },
  salt: {type: String},
  usertype: {type: Number},
//   usertype: [{
//     type:  mongoose.Schema.Types.ObjectId,
//     ref: usertypeModel
//     }],
  movie: [{
    type:  mongoose.Schema.Types.ObjectId,
    ref: movieModel
  }],
  review: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: reviewModel
  }]
});
const userModel = mongoose.model('user', userSchema);




const server = express()

server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())
server.use('/uploads', express.static('uploads'));
server.use('/useruploads', express.static('useruploads'));

server.post('/register', async (req, res) => {
  const payload = req.body;

  payload.salt = await bcrypt.genSalt(10)
  payload.password = await bcrypt.hash(payload.password, payload.salt)

  const newUser = new userModel(payload)
  const result = newUser.save()

  res.send({
    status: 200,
    data: result
  })
})

server.post('/login', async (req, res) => {
  const payload = req.body;

  let message = ''
  const userExist = await userModel.findOne({username: payload.username})
  if (!userExist) {
    message = 'user not found'
  };

  const isPasswordMatch = await bcrypt.compare(payload.password, userExist.password)
  if (!isPasswordMatch) {
    message = 'invalid password'
  }

  const token = await jwt.sign(userExist.toJSON(), 'secret_key')
  res.send({
    status: 200,
    message,
    data: {
      token
    }
  })
})

server.get('/users', async (req, res) => {
  const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');

  const user = await userModel.findOne({username: verifyToken.username}).populate({path: 'movie'})

  res.send({
    status: 200,
    data: user
  })
})

server.post('/userupdate', userupload.single('profilepicture'), async (req, res) => {
    console.log(req.file);
    const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
    const id = req.body.id;
    const profilepicture = req.file.path;

    userModel.findByIdAndUpdate(id, { profilepicture: profilepicture}, function(err, data) {
        if(err){
            console.log(err);
        } else {
            res.send({
              status: 200,
              data: userModel
            })
        }
    })
  })

server.post('/movie', upload.single('movieposter'), async (req, res) => {
    console.log(req.file);
  const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
  const title = req.body.title;
  const releaseyear = req.body.releaseyear;
  const releasedate = req.body.releasedate;
  const runtime = req.body.runtime;
  const genre = req.body.genre;
  const director = req.body.director;
  const writer = req.body.writer;
  const actor = req.body.actor;
  const plotsummary = req.body.plotsummary;
  const language = req.body.language;
  const country = req.body.country;
  //const movieposter = req.body.movieposter;
  const movieposter = req.file.path;
  const type = req.body.type;
  const studio = req.body.studio;
  const website = req.body.website;

  // get user first
  const user = await userModel.findOne({username: verifyToken.username})

  // insert post
  const newMovie = new movieModel({ title, releaseyear, releasedate, runtime, genre, director, writer, actor, plotsummary, language, country, movieposter, type, studio, website })
  await newMovie.save()

  user.movie.push(newMovie)
  await user.save()

  res.send({
    status: 200,
    data: newMovie
  })
})

server.post('/moviedelete', async (req, res) => {
    const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
    const id = req.body.id;
    const user = await userModel.findOne({username: verifyToken.username})
    console.log(id);
    movieModel.findByIdAndDelete(id, function(err, data) {
        if(err){
            console.log(err);
        } else {
            res.send({
              status: 200,
              data: movieModel
            })
        }
    })
})

server.post('/movieupdate', upload.single('movieposter'), async (req, res) => {
    console.log(req.file);
    const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
    const id = req.body.id;
    const title = req.body.title;
    const releaseyear = req.body.releaseyear;
    const releasedate = req.body.releasedate;
    const runtime = req.body.runtime;
    const genre = req.body.genre;
    const director = req.body.director;
    const writer = req.body.writer;
    const actor = req.body.actor;
    const plotsummary = req.body.plotsummary;
    const language = req.body.language;
    const country = req.body.country;
    //const movieposter = req.body.movieposter;
    const movieposter = req.file.path;
    const type = req.body.type;
    const studio = req.body.studio;
    const website = req.body.website;
    const user = await userModel.findOne({username: verifyToken.username})

    movieModel.findByIdAndUpdate(id, { title: title, releaseyear: releaseyear, releasedate: releasedate, runtime: runtime, genre: genre, director: director, writer: writer, actor: actor, plotsummary: plotsummary, language: language, country: country, movieposter: movieposter, type: type, studio: studio, website: website }, function(err, data) {
        if(err){
            console.log(err);
        } else {
            res.send({
              status: 200,
              data: movieModel
            })
        }
    })
  })

server.get('/movies', async (req, res) => {
    // destructure page and limit and set default values
    const { page = 1, limit = 10 } = req.query;
  
    try {
      // execute query with page and limit values
      const movies = await movieModel.find()
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
  
      // get total documents in the Posts collection 
      const count = await movieModel.countDocuments();
  
      // return response with posts, total pages, and current page
      res.json({
        movies,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      });
    } catch (err) {
      console.error(err.message);
    }
  });

server.post('/movie/:movieId/review', async (req, res) => {
  // code here
  // bikin handler untuk cek token ada atau engk
  // kalau engk ada kirim gini:
  // res.send({
  //   status: 400,
  //   message: 'no token',
  //   data: []
  // })

  const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
  const movieId = req.params.movieId;
  const review = req.body.review;
  const rating = req.body.rating;
  console.log(review);
try {
  if (!verifyToken) {
    res.send({
      status: 400,
      message: 'no token',
      data: []
    })
  }

  const movie = await movieModel.findOne({'_id': ObjectId(movieId)});
  
  // insert post
  const newReview = new reviewModel({ review, rating })
  await newReview.save();

  movie.review.push(newReview)
  await movie.save();

  const reviewing = await movieModel.findOne({'_id': ObjectId(movieId)}).populate({path: 'review'});

  res.send({
    status: 200,
    data: reviewing
  })
} catch (error) {
  console.log(error);
}
  
})

server.post('/reviewupdate', async (req, res) => {
    const verifyToken = jwt.verify(req.headers.authorization, 'secret_key');
    const id = req.body.id;
    const review = req.body.review;
    const rating = req.body.rating;

    reviewModel.findByIdAndUpdate(id, { review: review, rating: rating}, function(err, data) {
        if(err){
            console.log(err);
        } else {
            res.send({
              status: 200,
              data: reviewModel
            })
        }
    })
  })

  server.get('/reviewbymovie/:movieId', async (req, res) => {
    // destructure page and limit and set default values
    const { page = 1, limit = 10 } = req.query;
    const movieId = req.params.movieId;
  
    try {
      // execute query with page and limit values
      const review = await movieModel.findOne({'_id': ObjectId(movieId)}).populate({path: 'review'})
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
  
      // get total documents in the Posts collection 
      const count = await movieModel.countDocuments();
  
      // return response with posts, total pages, and current page
      res.json({
        review,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      });
    } catch (err) {
      console.error(err.message);
    }
  });

server.listen(3001, () => {
  console.log('SERVER RUNNING ON PORT 3001');
})
